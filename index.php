<?php 
session_start();
// подключение массивов с товарами и валютный
include_once $_SERVER['DOCUMENT_ROOT'].'/data/currency_arr.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/data/product_arr.php';

if(!empty($_SESSION['curren'])){
  $currency = $currencies[$_SESSION['curren']];
}else{
  $currency = $currencies['uah'];
};

function getPriceWithDiscount($price_val, $discount_type, $discount_val) {
  if($discount_type == 'value'){
    $price = $price_val - $discount_val;
    return round($price, 2);
  }elseif($discount_type == 'percent'){
    $price = $price_val*((100-$discount_val)/100);
    return round($price, 2);
  };
};
//переменная для второго агрумента
$course = $currency['course'];
function convertPrice($price_val, $course){
  $priceEnd = round($price_val / $course, 2);
  return $priceEnd;
};
?>
<!-- подключение html файлов -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/html/header_connect.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/html/header.php';?>
  <div class="container">
    <div class="row">
      <!-- выводим все товары в таблицу -->
      <?php foreach($products as $product):?>
        <div class="col-5">
          <div class="card">
            <div class="card-body">
              <h4><?=$product['title']?></h4>
              <p><?=$product['price_val'];?></p>
              <p>price with discount</p>
              <!-- стоимость с учетом скидки и выбором валюты -->
              <p><?=convertPrice(getPriceWithDiscount($product['price_val'], $product['discount_type'], 
                $product['discount_val']),$currency['course']).' '.$currency['name'];?>
              </p>
            </div>
          </div>
        </div>
      <?php endforeach;?>    
      <div class="col-2">
        <form action="/session.php" method="post">
          <div class="mb-3">
            <label class="form-label" name="curren">Currency</label>
            <select class="form-control" name="curren">
              <option value="uah">Гривна</option>
              <option value="usd">Доллар</option>
              <option value="eur">Евро</option>
            </select>
          </div>
        <div class="mb-3">
          <button class="btn btn-primary">Валюта</button>
        </div>
      </form>
    </div>
  </div>
  </div>  
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/html/footer.php';?>
  
