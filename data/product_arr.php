<?php
$products =[
    [
       'title' =>'Dior Sauvage Eau de Parfum 30ml',
       'price_val' =>1240,
       'discount_type' => 'value',
       'discount_val' => 140,
    ],
    [
       'title' =>'Dior Jadore 30ml',
       'price_val' =>1580,
       'discount_type' => 'percent',
       'discount_val' => 10,
    ],
    [
       'title' =>'Dior Joy By Dior 30ml',
       'price_val' =>1640,
       'discount_type' => 'value',
       'discount_val' => 100,
    ],
    [
       'title' =>'Dior Addict Eau de Parfum 2014 30 ml',
       'price_val' =>1730,
       'discount_type' => 'percent',
       'discount_val' => 10,
    ],
    [
       'title' =>'Dior JAdore LAbsolu 50 ml',
       'price_val' =>2000,
       'discount_type' => 'value',
       'discount_val' => 200,
    ],
    [
       'title' =>'Dior Poison Girl 30 ml',
       'price_val' =>2050,
       'discount_type' => 'percent',
       'discount_val' => 15,
    ],
    [
       'title' =>'Dior Sauvage Eau de Parfum 30 ml',
       'price_val' =>2340,
       'discount_type' => 'value',
       'discount_val' => 140,
    ],
    [
       'title' =>'Dior Joy by Dior Intense 50 ml',
       'price_val' =>2570,
       'discount_type' => 'value',
       'discount_val' => 150,
    ],
    [
       'title' =>'Dior Homme Intense 100 ml',
       'price_val' =>2750,
       'discount_type' => 'percent',
       'discount_val' => 15,
    ], 
    [
       'title' =>'Dior J\'Adore Infinissime 100 ml',
       'price_val' =>2900,
       'discount_type' => 'value',
       'discount_val' => 200,
    ]
  ];
  
?>