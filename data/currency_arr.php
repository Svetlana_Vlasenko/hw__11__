<?php
$currencies = [
   'uah' => [
      'name' => 'Гривна',
      'course' => 1,
   ],
   'usd' => [
      'name' => 'Доллар',
      'course' => 27.1,
   ],
   'eur' => [
      'name' => 'Евро',
      'course' => 30.2,
   ],
];
?>